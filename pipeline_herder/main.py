"""Pipeline herder main module."""
import argparse
import datetime
import re
import sys
from urllib import parse

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
import dateutil
import prometheus_client as prometheus
import sentry_sdk

from . import matchers
from . import settings
from . import utils

LOGGER = get_logger('cki.pipeline_herder.main')

METRIC_MESSAGE_DELAYED = prometheus.Counter(
    'message_delayed', 'Number of queue messages delayed via retry queue')
METRIC_PROBLEM_DETECTED = prometheus.Counter(
    'herder_problem_detected', 'Number of problems detected', ['matcher'])
METRIC_PROBLEM_RETRIES = prometheus.Histogram(
    'herder_problem_retries', 'Number of retries for a job with a problem', ['matcher'])
METRIC_NO_PROBLEM_DETECTED = prometheus.Counter(
    'herder_no_problem_detected', 'Number of jobs without a detected problem')
METRIC_PROBLEM_REPORTED = prometheus.Counter(
    'herder_problem_reported', 'Number of problems reported', ['matcher'])
METRIC_PROBLEM_RETRIED = prometheus.Counter(
    'herder_problem_retried', 'Number of problems retried', ['matcher'])
METRIC_PROCESS_TIME = prometheus.Histogram(
    'herder_process_time_seconds', 'Time spent matching a job')


def process_webhook(body=None, ack_fn=None, **_):
    """Process a job from a GitLab webhook."""
    object_kind = body['object_kind']
    if object_kind == 'retry_trigger':
        result = handle_retry_trigger(body)
    elif object_kind == 'build':
        result = handle_build(body)
    else:
        LOGGER.info('Ignoring %s for %s', object_kind, body.get('project_name', 'unknown'))
        result = True
    if not result:
        METRIC_MESSAGE_DELAYED.inc()
    ack_fn(result)


def handle_retry_trigger(message):
    """Handle a pipeline-herder retry message."""
    if datetime.datetime.now().astimezone() < dateutil.parser.parse(message['not_before']):
        return False
    retry(message['gitlab_host'], message['project'], message['job_id'])
    return True


def handle_build(message):
    """Handle a GitLab job webhook."""
    status = message['build_status']
    if status in ('success', 'failed'):
        homepage = parse.urlparse(message['repository']['homepage'])
        process_job(homepage[1], homepage[2].lstrip('/'), message['build_id'])
    else:
        LOGGER.info('Ignoring %s for %s', status, message["project_name"])
    return True


@METRIC_PROCESS_TIME.time()
def process_job(gitlab_host, project, job_id):
    """Process a job directly specified via host/project/id."""
    job = utils.CachedJob(gitlab_host, project, job_id)
    job_id = job.gl_job.id
    LOGGER.info('Processing P%s J%s', job.gl_pipeline.id, job_id)

    matcher_instance = matchers.match(job)

    if not matcher_instance or matcher_instance.always_notify:
        # Send message about finished job.
        notify_finished(gitlab_host, project, job_id)

    if not matcher_instance:
        # No problem found, nothing else to do.
        METRIC_NO_PROBLEM_DETECTED.inc()
        return None

    affected_job = matcher_instance.get_affected_job(job)
    description = matcher_instance.description

    METRIC_PROBLEM_DETECTED.labels(matcher=matcher_instance.name).inc()
    METRIC_PROBLEM_RETRIES.labels(matcher=matcher_instance.name).observe(
        affected_job.job_name_count()
    )

    if matcher_instance.action == 'report':
        utils.notify_irc(affected_job, f'Detected {description}')
        METRIC_PROBLEM_REPORTED.labels(matcher=matcher_instance.name).inc()
        return 'report'

    if matcher_instance.action == 'retry':
        unsafe_reason = affected_job.is_retry_unsafe()
        if unsafe_reason:
            utils.notify_irc(affected_job, f'Detected {description}, '
                             f'not retrying: {unsafe_reason}')
            METRIC_PROBLEM_REPORTED.labels(matcher=matcher_instance.name).inc()
            return 'report'
        delay = affected_job.retry_delay()
        if delay == 0:
            utils.notify_irc(affected_job, f'Detected {description}, retrying now')
        else:
            utils.notify_irc(affected_job, f'Detected {description}, '
                             f'retrying in {delay} minutes')
        submit_retry(affected_job, matcher_instance, delay)
        METRIC_PROBLEM_RETRIED.labels(matcher=matcher_instance.name).inc()
        return 'retry'

    utils.notify_irc(
        affected_job, (f'Detected {description}. '
                       f'🔥 Action \'{matcher_instance.action}\' not handled 🔥')
    )
    return 'error'


def notify_finished(gitlab_host, project, job_id):
    """Add finished job to the message queue."""
    if not misc.is_production():
        LOGGER.info('Not notifying via amqp because of non-production env')
        return
    messagequeue.MessageQueue().send_message(
        {
            'gitlab_url': gitlab_host,
            'project': project,
            'job_id': job_id,
        },
        settings.RABBITMQ_PUBLISH_ROUTING_KEY,
        exchange=settings.RABBITMQ_PUBLISH_EXCHANGE
    )


def submit_retry(job, _, delay):
    """Submit the suggested actions with the specified delay in minutes."""
    not_before = datetime.datetime.now().astimezone() + datetime.timedelta(minutes=delay)
    messagequeue.MessageQueue().send_message({
        'object_kind': 'retry_trigger',
        'gitlab_host': job.gitlab_host,
        'project': job.project,
        'job_id': job.job_id,
        'not_before': not_before.isoformat(),
    }, settings.RABBITMQ_WEBHOOKS_QUEUE)


def retry(gitlab_host, project, job_id):
    """Safely retry a job."""
    job = utils.CachedJob(gitlab_host, project, job_id)
    unsafe_reason = job.is_retry_unsafe()
    if unsafe_reason:
        utils.notify_irc(job, f'Not retried, {unsafe_reason}')
    else:
        if job.gl_job.status == 'running':
            job.gl_job.cancel()
        job.gl_job.retry()
        utils.notify_irc(job, 'Retried')


def process_queue():
    """Process jobs from the message queue."""
    misc.sentry_init(sentry_sdk)

    messagequeue.MessageQueue().consume_messages(
        settings.RABBITMQ_WEBHOOKS_EXCHANGE,
        settings.RABBITMQ_WEBHOOKS_ROUTING_KEYS,
        process_webhook,
        callback_kwargs=True,
        queue_name=settings.RABBITMQ_WEBHOOKS_QUEUE,
        manual_ack=True)


def process_single(job_url):
    """Process a single job from the command line."""
    url_parts = parse.urlparse(job_url)
    gitlab_host = url_parts.hostname
    project = re.sub('/-/.*', '', url_parts.path[1:])
    job_id = re.sub('.*/', '', url_parts.path)
    job = utils.CachedJob(gitlab_host, project, job_id)
    matcher_instance = matchers.match(job)
    if matcher_instance:
        print(matcher_instance.description)
        return
    print('Nothing matched 😕')


def main(argv):
    """CLI Interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--job-url',
                        help='Try the matcher for an individual job.')
    args = parser.parse_args(argv)

    metrics.prometheus_init()

    if args.job_url:
        process_single(args.job_url)
    else:
        process_queue()


if __name__ == '__main__':
    main(sys.argv[1:])
