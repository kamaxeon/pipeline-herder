"""Various matchers."""
import re

from . import utils

MATCHERS = [
    utils.Matcher(
        name='code137',
        description='Pod resource exhaustion (exit code 137)',
        messages=['ERROR: Job failed: command terminated with exit code 137',
                  'ERROR: Job failed: exit code 137',
                  'No space left on device',
                  'Disk quota exceeded']
    ),
    utils.Matcher(
        name='code143',
        description='Pod killed by SIGTERM (exit code 143)',
        messages=['ERROR: Job failed: command terminated with exit code 143']
    ),
    utils.Matcher(
        name='image-pull',
        description='Failure during image pull',
        messages='ERROR: Job failed: image pull failed',
    ),
    utils.Matcher(
        name='system-failure',
        description='System failure',
        messages='ERROR: Job failed (system failure)',
    ),
    utils.Matcher(
        name='ceph-s3',
        description='Problem accessing Ceph S3 buckets',
        messages=[
            re.compile(
                r'(download|upload) failed.*s3://' +
                r'(cki|DH-PROD-CKI|DH-SECURE-CKI)' +
                r'.*(Connection was closed before we received a valid' +
                r' response|AccessDenied|Bad Request)'),
            re.compile(
                r'Connection was closed before we received a valid ' +
                r'response from endpoint URL: ' +
                r'"https://s3.upshift.redhat.com/DH-SECURE-CKI'),
        ]
    ),
    utils.Matcher(
        name='network-dns',
        description='Network troubles - DNS',
        messages=[
            'Name or service not known',
            'Could not resolve host'
        ]
    ),
    utils.Matcher(
        name='network-instance',
        description='Network troubles reaching the GitLab instance',
        messages=[
            re.compile(r'ERROR: Uploading artifacts.*FATAL: invalid argument', re.DOTALL),
            re.compile(r'WARNING: Downloading artifacts.*failed.*502 Bad Gateway'),
        ]
    ),
    utils.Matcher(
        name='network-gitlab-com',
        description='Network troubles reaching gitlab.com',
        messages=re.compile(
            r'ERROR: Command errored out with exit status 128: git clone.*https://gitlab.com')
    ),
    utils.Matcher(
        name='network-generic',
        description='Generic network trouble',
        messages='TLS handshake timeout'
    ),
    utils.Matcher(
        name='skewed-time',
        description='Node has wrong time set',
        messages=['An error occurred (RequestTimeTooSkewed) when calling']
    ),
    utils.Matcher(
        name='missing-git-cache',
        description='Missing git cache (manual intervention needed!)',
        messages=re.compile(
            r'download failed: s3://cki/git-cache/.* error occurred '
            r'\(NoSuchKey\) when calling the GetObject operation')
    ),
    utils.Matcher(
        name='process-limit',
        description='Unable to fork',
        job_name='build',
        file_name='artifacts/build.log',
        tail_lines=500,
        messages='Resource temporarily unavailable',
    ),
    utils.Matcher(
        name='datawarehouse-down',
        description='connection to DataWarehouse was refused',
        job_name='check-kernel-results',
        messages=[
            'requests.exceptions.HTTPError: 404 Client Error: Not Found for url: '
            'https://datawarehouse.internal.cki-project.org/api/1/kcidb',
            'Failed to establish a new connection: [Errno 110] Connection timed out',
        ],
    ),
    utils.Matcher(
        name='force-push',
        description='Can not checkout the remote reference',
        messages=re.compile(
            r'fatal: reference is not a tree:'),
        action='report',
    ),
    utils.Matcher(
        name='too-large',
        description='Artifacts size exceeded',
        messages=re.compile(
            r'ERROR: Uploading artifacts( as "archive")? to coordinator... too large'),
        action='report',
    ),
    utils.Matcher(
        name='old-git',
        description='Old git versions cannot handle URLs without .git suffix'
                    '(manual intervention needed!)',
        messages=re.compile(
            r'error: RPC failed; result=22, HTTP code = 422.*'
            r'fatal: The remote end hung up unexpectedly'),
        action='report',
    ),
    utils.Matcher(  # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27022
        name='volume-cleanup-failed',
        description='gitlab-runner failed cleaning up files, might bleed into future jobs',
        messages='Failed to cleanup volumes',
        job_status=('failed', 'success'),
        action='report',
        always_notify=True,
    ),
    utils.RCMatcher(
        name='missing-merge-result',
        description='The merge has no merge result',
        checks={
            'state/stage_merge': re.compile(r'pass|fail|skip'),
        },
        job_name='merge',
        action='report'
    ),
    utils.RCMatcher(
        name='missing-build-result',
        description='The build has no build result',
        checks={
            'state/stage_build': re.compile(r'pass|fail'),
        },
        job_name='build ',  # The space is necessary to difference it from build_tools
        action='report'
    ),
    utils.RCMatcher(
        name='missing-build-tools-result',
        description='The build_tools has no build result',
        checks={
            'state/stage_build': re.compile(r'pass|fail'),
        },
        job_name='build_tools',
        action='report',
        rc_path='artifacts/build-tools.rc'
    ),
    utils.RCMatcher(
        name='missing-test-result',
        description='The test has no rc file',
        checks={
            # Dummy check to guarantee there's an rc file
        },
        job_name='test',
        action='report'
    ),

    # Keep it as a fallback to avoid masking other problems, just in case.
    utils.NoTraceMatcher(),
]


def match(job):
    """Compare a job against the list of matchers and return the first match."""
    return next((m for m in MATCHERS if m.check(job)), None)
